package com.manko.task06.part01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        myFunctionalInterface lambda1 = (a, b, c) -> (a > b && a > c) ? a : (b > a && b > c) ? b : c;
        int maxNumber = lambda1.mathMethod(6, 1, 3);
        logger.info("The maximum number is " + maxNumber);

        myFunctionalInterface lambda2 = (a, b, c) -> (a + b + c) / 3;
        int average = lambda2.mathMethod(1, 0, 1);
        logger.info("The average value is " + average);
    }
}

@FunctionalInterface
interface myFunctionalInterface {
    int mathMethod(int a, int b, int c);
}
