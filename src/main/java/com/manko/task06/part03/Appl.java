package com.manko.task06.part03;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Appl {

    private static Logger logger = LogManager.getLogger(Appl.class);

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Appl app = new Appl();
        List<Integer> list1 = app.createListByMathRandom();
        List<Integer> list2 = app.createListByRandom();
        List<Integer> list3 = app.createListBNextInt();
        logger.info("list1:" + list1);
        logger.info("List2:" + list2);
        logger.info("List3:" + list3);
        int sum1 = app.getSumOfStreamElements(list1).get();
        logger.info("sum = " + sum1);
        int sum2 = app.getSumOfIntStreamElements(list1);
        logger.info("sum = " + sum2);
        int max = app.getMaxElement(list1);
        logger.info("max = " + max);
        int min = app.getMinElement(list1);
        logger.info("min = " + min);
        double average = app.getAverage(list1);
        logger.info("average = " + average);
        long count = app.countBiggerValue(list1);
        logger.info("count = " + count);
    }

    private List<Integer> createListByMathRandom() {
        List<Integer> list = Stream.generate(Math::random)
                .limit(10)
                .map(a -> (int) (a * 10))
                .collect(Collectors.toList());
        return list;
    }

    private List<Integer> createListByRandom() {
        List<Integer> list = Stream.generate(new Random()::nextInt)
                .limit(10)
                .collect(Collectors.toList());
        return list;
    }

    private List<Integer> createListBNextInt() {
        int start = new Random().nextInt(10);
        List<Integer> list = Stream.iterate(start, n -> n * 2)
                .limit(10)
                .collect(Collectors.toList());
        return list;
    }
    
    private Optional<Integer> getSumOfStreamElements(List<Integer> list) {
        Optional<Integer> sum = list.stream().reduce((a, b) -> a + b);
        return sum;
    }

    private int getSumOfIntStreamElements(List<Integer> list) {
        int sum = list.stream().mapToInt(x -> x).sum();
        return sum;
    }

    private int getMaxElement(List<Integer> list) {
        int max = list.stream().mapToInt(x -> x).max().getAsInt();
        return max;
    }

    private int getMinElement(List<Integer> list) {
        int min = list.stream().mapToInt(x -> x).min().getAsInt();
        return min;
    }

    private double getAverage(List<Integer> list) {
        double average = list.stream().mapToInt(x -> x).average().getAsDouble();
        return average;
    }

    private long countBiggerValue(List<Integer> list) {
        double averageValue = getAverage(list);
        long count = list.stream().filter(a -> a > averageValue).count();
        return count;
    }

    private void printList(List<Integer> list) {
        list.stream().forEach(a -> System.out.print(a + " "));
        System.out.println();
    }
}
