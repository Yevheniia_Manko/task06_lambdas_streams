package com.manko.task06.part02;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class UserMenu {

    private static final String INVALID_COMMAND = "Invalid command: ";
    private static final String INCORRECT_INPUT = "Incorrect input: ";
    private static Logger logger = LogManager.getLogger(UserMenu.class);
    private Map<String, String> menu;
    private static Scanner sc = new Scanner(System.in);
    private String command = "";
    private String userString = "";

    public UserMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Lambda command");
        menu.put("2", " 2 - Method reference command");
        menu.put("3", " 3 - Anonymous class command");
        menu.put("4", " 4 - Command class implementation");
        menu.put("Q", " Q - Exit");
    }

    public void show() {
        do {
            outputMenu();
            readUserInput();
            if (userString.isEmpty() && !command.equalsIgnoreCase("Q")) {
                logger.info(INCORRECT_INPUT);
                logger.warn(INCORRECT_INPUT + command);
                continue;
            }
            switch (command) {
                case "1":
                    Command commandOne = (String a) -> logger.info(a);
                    commandOne.execute(userString);
                    break;
                case "2":
                    Command commandTwo = logger::info;
                    commandTwo.execute(userString);
                    break;
                case "3":
                    Command commandThree = new Command() {
                        @Override
                        public void execute(String s) {
                            logger.info(s);
                        }
                    };
                    commandThree.execute(userString);
                    break;
                case "4":
                    Command commandFour = new CommandFour();
                     commandFour.execute(userString);
                    break;
                case "Q":
                    break;
                default:
                    logger.info(INVALID_COMMAND);
                    logger.warn(INVALID_COMMAND + command + " " + userString);
                    break;
            }
        } while (!command.equalsIgnoreCase("Q"));
    }

    private void readUserInput() {
        String userInput;
        command = "";
        userString = "";
        logger.info("Please input menu point and the string argument:");
        userInput = sc.nextLine();
        String[] strings = userInput.trim().split(" ");
        int inputSize = strings.length;
        command = strings[0].toUpperCase();
        if (inputSize >= 2) {
            userString = strings[1];
            if (inputSize > 2) {
                for (int i = 2; i < strings.length; i++) {
                    userString = userString + " " + strings[i];
                }
            }
        }
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
