package com.manko.task06.part02;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandFour implements Command {

    private static Logger logger = LogManager.getLogger(CommandFour.class);

    public CommandFour() {
    }

    @Override
    public void execute(String message) {
        logger.info(message);
    }
}
