package com.manko.task06.part02;

public interface Command {
    void execute(String s);
}
