package com.manko.task06.part04;

import com.manko.task06.part04.view.UserConsoleView;

public class Application {

    public static void main(String[] args) {
        UserConsoleView view = new UserConsoleView();
        view.start();
    }
}
