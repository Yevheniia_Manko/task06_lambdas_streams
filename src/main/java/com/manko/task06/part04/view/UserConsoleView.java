package com.manko.task06.part04.view;

import com.manko.task06.part04.controller.TextController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class UserConsoleView {

    private TextController controller;

    private static Logger logger = LogManager.getLogger(UserConsoleView.class);
    private static Scanner sc = new Scanner(System.in);

    public UserConsoleView() {
        controller = new TextController();
    }

    public void start() {
        readUserInput();
        boolean oneMore = true;
        while (oneMore) {
            int choice = mainMenu();
            switch (choice) {
                case 0:
                    oneMore = false;
                    break;
                case 1:
                    printWordCount();
                    break;
                case 2:
                    printUniqueWordsCount();
                    break;
                case 3:
                    printUniqueWordsList();
                    break;
                case 4:
                    printWordsOccurrenceNumber();
                    break;
                case 5:
                    printSymbolOccurrenceNumber();
                    break;
                default:
                    logger.info("You entered invalid value.");
                    logger.info("Please enter the mainMenu item.");
                    break;
            }
        }
    }

    private void readUserInput() {
        logger.info("Please input the text:");
        List<String > userText = new ArrayList<>();
        String userInput;
        int i = 0;
        do {
            userInput = sc.nextLine();
            if (userInput != null && !userInput.equals("")) {
                userText.add(userInput);
            }
        }
        while (!userInput.equals(""));
        controller.createText(userText);
    }

    private int mainMenu() {
        logger.info("\nMENU:");
        logger.info("1. Print number of all words.");
        logger.info("2. Print number of unique words.");
        logger.info("3. Print a list of unique words.");
        logger.info("4. Print word occurrence number.");
        logger.info("5. Print symbol occurrence number except upper case symbols.");
        logger.info("0. Exit");
        System.out.println("Input your choice:");
        return readIntegerValue();
    }

    private int readIntegerValue() {
        int value;
        while (true) {
            try {
                value = Integer.parseInt(sc.nextLine());
                break;
            } catch (Exception e) {
                logger.info("You entered invalid value.");
                logger.info("Please try again.");
            }
        }
        return value;
    }

    private void printWordCount() {
        long count = controller.showWordsCount();
        logger.info("The count of all words is " + count);
    }

    private void printUniqueWordsCount() {
        long count = controller.showUniqueWordsCount();
        logger.info("The count of unique words is " + count);
    }

    private void printUniqueWordsList() {
        List<String> list = controller.getUniqueWordList();
        logger.info("The list of unique words: " + list);
    }

    private void printWordsOccurrenceNumber() {
        logger.info("The occurrence count of each word:");
        Map<String, Long> map = controller.getWordOccurrenceNumber();
        for (Map.Entry<String, Long> pair : map.entrySet()) {
            logger.info(pair.getKey() + "  " + pair.getValue());
        }
    }

    private void printSymbolOccurrenceNumber() {
        logger.info("The occurrence count of each symbol except upper case symbols:");
        Map<String, Long> map = controller.getSymbolsOccurrenceNumber();
        for (Map.Entry<String, Long> pair : map.entrySet()) {
            logger.info(pair.getKey() + "  " + pair.getValue());
        }
    }
}
