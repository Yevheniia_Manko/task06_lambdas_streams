package com.manko.task06.part04.controller;

import com.manko.task06.part04.model.Text;

import java.util.List;
import java.util.Map;

public class TextController {

    private Text text;

    public TextController() {
        text = new Text();
    }

    public void createText(List<String> list) {
        text.setText(list);
    }

    public List<String> getTextLines() {
        return text.getText();
    }

    public long showWordsCount() {
        return text.getAllWordsCount();
    }

    public long showUniqueWordsCount() {
        return text.getUniqueWordsCount();
    }

    public List<String> getUniqueWordList() {
        return text.getUniqueWordsList();
    }

    public Map<String, Long> getWordOccurrenceNumber() {
        return text.getOccurrenceNumber();
    }

    public Map<String, Long> getSymbolsOccurrenceNumber() {
        return text.getSymbolsOccurrenceNumber();
    }
}
