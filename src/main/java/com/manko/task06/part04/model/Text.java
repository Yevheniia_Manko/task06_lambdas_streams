package com.manko.task06.part04.model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Text {

    private List<String> text;

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public long getAllWordsCount() {
        return text.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .filter(s -> !s.equals(""))
                .count();
    }

    public long getUniqueWordsCount() {
        return text.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .filter(s -> !s.equals(""))
                .distinct()
                .count();
    }

    public List<String> getUniqueWordsList() {
        List<String> uniqueWordsList = text.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .filter(s -> !s.equals(""))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        return uniqueWordsList;
    }

    public Map<String, Long> getOccurrenceNumber() {
        Map<String, Long> map = text.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .filter(s -> !s.equals(""))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        return map;
    }

    public Map<String, Long> getSymbolsOccurrenceNumber() {
        Map<String, Long> symbolsMap = text.stream()
                .flatMap(s -> Stream.of(s.split("")))
                .filter(s -> !s.equals(" "))
                .filter(s -> (!Character.isUpperCase(s.charAt(0))))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        return symbolsMap;
    }
}
